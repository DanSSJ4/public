# Ships
The current meta is now that CSV > FAW  
Take this guide as very simplified version, yeah, it's for you tl;dr guys!  
It's some kick-off start so you can farm before you'll get some expensive stuff which is advised on the internet

## Ship equipment
Some tips to start about loadout:

![basic loadout tips](resources/ship-setup.png)

Use all energy weapons of same type. You might be interested by reputation (or any other) set weapons if they fit in

## Boffs

### Tactical
- The ability pick depends on the weapon set picked (if you picked torpedo, use the variant with torpedo abilities, if not, don't use torp abilities)
- Check your ship and count its total tac abilities and look below:

#### 5 tac boff abilities total
- Don't use torpedo weapons
- 2x TT, 2x CSV, 1x APB

#### 6 tac boff abilities total
- Don't use torpedo weapons
- 2x TT, 2x CSV, 2x APB

#### 7 tac boff abilities total
- 2x TT, 2x CSV, 2x TS, 1x APB (with torpedo)  
or
- 2x TT, 2x CSV, 2x APO, 1x APB (without torpedo)

#### 8 tac boff abilities total
- 2x TT, 2x CSV, 2x TS, 2x APB (with torpedo)  
 or
- 2x TT, 2x CSV, 2x APO, 1x APB, 1x kemo (without torpedo, but getting kemo can be expensive)

#### 9 tac boff abilities total
- 2x TT, 2x CSV, 25x TS 2x APO, 1x APB (with torpedo)

#### Preferred ability grades:
Stick to those if possible:
- TT: 1
- APB: 3
- CSV: 1
- TS: 3 (but sometimes you must use 1 due to boff layout)
- APO: 1 or 3
- kemo: 1

### Engineering
- pick some heals like ET, EPTS, RSP

### Science
- pick GW if you can
- pick again some heals like HE, TSS, ST

## Free ships picks
### Federation
Regardless of your career:
- L10: Escort
- L20: Heavy Escort
- L30: Tactical Escort
- L40: Advanced Escort
- L61: Tactical Escort Retrofit  

Why this way? You won't tank well with Cruisers and Sci vessels are a meh without expensive stuff. So you'll have to deal some dmg (if you want to survive, get rid of that thing that is damaging you)
### Romulan
- L10: Dhelan (only option)
- L20: Mogai (only option)
- L30: Ar'Kif
- L40: Ha'feh
- L61: Mogai or Dhelan

### Dominion
- The L61 pick depends on your ally

### KDF
- L10: Somraw
- L20: K't'inga 
- L30: Pach
- L40: Qin (Vo'quv is an option if a sci)
- L61: Kar'fi
