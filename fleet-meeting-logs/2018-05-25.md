# Fleet meeting summary log

## Attendees
- Morgana (aka Ziktofel)
- Cherry
- Nixoa (aka Brea)
- Branden
- Alandrii (left after promotion discussion)
- Sh'aass

## Armada
- No candidate for our empty gamma slot. Need to do some search
- Outgoing dil contributions (not incoming) disabled to prevent misclicks

## Projects
- Manual gathering of colony supplies not needed
- Fleet spire T3 finish project finally going

## Help needed
- We need to help with some manpower
- Recruiters and event organizers are preferred
- Cherry has more busy period in the work therefore wants to some tasks delegated

### Requirements/Tasks
- Recruiter:
  - Recruits new fleet members
- Event organizer:
  - Makes fleet events
  - Attends created events
  - Counts attendees
  - Mekes fleet fun

## Promotion rules (all must apply)
- Lt → Lt Commander:
  - conversation in fleet chat
  - attend to a fleet event
- Others are to be specified by Cherry

## Meeting organization
- As the attendee count increases and the table has only 7 chairs we shall take the next meeting at the embassy
